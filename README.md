# DevOps Junior - Newway

## Desafio

Gostariamos que você colocasse uma url Hello-World com HTTPS (Letsencrypt) e Nginx, utilizando CI/CD do Gitlab ou outra ferramenta de sua preferência, de preferência utilizando a AWS, a aplicação precisa ser configurado com container Docker em uma EC2.

Esse processo tem objetivo avaliar o seu conhecimento básico em DNS, AWS e seus recursos basicos, Docker, Nginx (proxy-pass) e Git.


## Para isso

 - Criar uma conta gratuita na AWS e nos disponibilizar acesso de leitura;
 - Criar uma EC2;
 - Criar uma pagina Hello World exposta em HTTPS;
 - Você precisa expor URL em HTTPS com Letsencrypt usando NGINX com proxy-pass (procurar por URL gratuita como freenom ou similiar);
 - Todo processo precisa ser feito utilizando Gitlab com CI/CD (Build e Deploy);
 - Criar e Configurar um Grafana e monitorar a instância com o datasource do Cloudwatch;
 - Documentar como funciona a sua aplicação e um desenho de arquitetura/infraestrutura em um README.md;

## Diferenciais
 - Infraestrutura como Código (Terraform, Codebuild ou Palumi);
 - Criar algum tipo de automação nesse processo com python ou shell script;
 - Utilizar outros recursos da AWS do que os propostos;
 - Documentação bem elabadorada e detalhada;
 - Nos surpreenda.
